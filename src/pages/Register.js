import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Register() {


	const { user, setUser } = useContext(UserContext);
	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState('');

	const history = useHistory();

	function registerUser(e) {

		e.preventDefault();


		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if (data === true) {
					Swal.fire({
						title: 'Email already registered',
						icon: 'error',
						text: 'This email is already registered. Please try a different email.'
					})
				} else {
					fetch('http://localhost:4000/users/register', {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							mobileNo: mobileNo,
							email: email,
							password: password
						})
					})
						.then(res => res.json())
						.then(data => {
							console.log(data)

							if (data === true) {

								Swal.fire({
									title: 'Registered Succesfully',
									icon: 'success',
									text: 'Welcome to Zuitt'
								})
							}

							history.push('/login')
						})
				}
			})

		//Prevents page redirection via form submission

	}


	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match
		if ((email !== '' && password !== '' && password2 !== '') && (password === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, password, password2]);


	return (
		(user.id !== null) ?
			<Redirect to='/courses' />
			:
			<Form onSubmit={(e) => registerUser(e)}>
				<Form.Group controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="First Name"
						value={firstName}
						onChange={e => setFirstName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Last Name"
						value={lastName}
						onChange={e => setLastName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="mobileNo">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control
						type="text"
						placeholder="Mobile #"
						value={mobileNo}
						onChange={e => setMobileNo(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="userEmail">
					<Form.Label>Email address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Verify Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify Password"
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{/* conditionally render submit button based on isActive state */}
				{isActive ?
					<Button variant="primary" type="submit" id="submitBtn">
						Submit
					</Button>
					:
					<Button variant="danger" type="submit" id="submitBtn" disabled>
						Submit
					</Button>
				}

			</Form>
	)

}
