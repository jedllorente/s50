import { useState, useEffect, useContext } from 'react'
import { Card, Col, Container, Row, Button } from "react-bootstrap";
import { useParams, useHistory, Link } from 'react-router-dom'
//useHistory for v5 below on react-router-dom
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function CourseView() {

    //useParams hook allows us to retreive the courdeId passed via the URL
    const { courseId } = useParams()


    const { user } = useContext(UserContext)

    // useHistory hook allows us to redirect user to a different page.
    const history = useHistory()

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState(0)

    const enroll = (courseId, userId) => {
        fetch(`http://localhost:4000/users/enroll`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                courseId: courseId,
                userId: userId
            })
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data)

                if (data === true) {
                    Swal.fire({
                        title: 'Enrolled Succesfully!',
                        icon: 'success',
                        text: 'You have successfully enrolled this course.'
                    })
                    history.push("/courses")
                } else {
                    Swal.fire({
                        title: 'Unable to enroll this course',
                        icon: 'error',
                        text: 'Please try again.'
                    })
                }
            })
    }

    useEffect(() => {
        console.log(courseId)

        fetch(`http://localhost:4000/courses/${courseId}`)
            .then(res => res.json())
            .then(data => {
                console.log(data)

                setName(data.name)
                setDescription(data.description)
                setPrice(data.price)
            })
    }, [courseId])
    return (
        <Container>
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className='text-center'>
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>Php {price}</Card.Text>
                            <Card.Subtitle>Class Schedule:</Card.Subtitle>
                            <Card.Text>5:30pm to 9:30pm</Card.Text>
                            {user.id !== null ?
                                <Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
                                :
                                <Link className="btn btn-danger btn-black" to="/login">Enroll</Link>
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}